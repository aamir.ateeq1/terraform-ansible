terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.5.0"
    }
  }
}

provider "google" {
  credentials = file("/home/skuad/keys/key.json")
  project = "stunning-hue-312911"
  region  = "asia-east1"
  zone    = "asia-east1-a"
}

resource "google_compute_instance" "vm_instance" {
  name         = "terraform-instance-abintio-v1"
  machine_type = "e2-standard-16"

 boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1804-lts"
      size = "200"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
}


resource "null_resource" "readcontentfile" {
  provisioner "local-exec" {
    command = "sh /var/lib/jenkins/workspace/Launch-Demo-Setup/terraform/run.sh"
  }
  depends_on = [google_compute_instance.vm_instance]
}

